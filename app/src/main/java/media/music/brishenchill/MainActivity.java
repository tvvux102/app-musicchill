package media.music.brishenchill;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.ActionMode;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textViewTitle;
    TextView textViewTimeRun;
    TextView textViewTimeSum;
    SeekBar seekBarTime;
    ImageButton imageButtonPrev;
    ImageButton imageButtonPlay;
    ImageButton imageButtonStop;
    ImageButton imageButtonNext;
    ImageView imageViewCD;
    ArrayList<Song> arraySong;
    int position = 0;
    MediaPlayer mediaPlayer;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        AddSong();
        animation = AnimationUtils.loadAnimation(this, R.anim.disc_rotate);
        MediaPlayer();

        imageButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position++;
                if (position > arraySong.size() - 1) {
                    position = 0;
                }
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                MediaPlayer();
                mediaPlayer.start();
                imageButtonPlay.setImageResource(R.drawable.ic_pause);
                SetTimeSum();
            }
        });

        imageButtonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position--;
                if (position < 0) {
                    position = arraySong.size() - 1;
                }
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                MediaPlayer();
                mediaPlayer.start();
                imageButtonPlay.setImageResource(R.drawable.ic_pause);
                SetTimeSum();
                UpdateTimeSong();
            }
        });


        imageButtonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.stop();
                mediaPlayer.release();
                imageButtonPlay.setImageResource(R.drawable.ic_play);
                MediaPlayer();
            }
        });

        imageButtonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    imageButtonPlay.setImageResource(R.drawable.ic_play);
                } else {
                    mediaPlayer.start();
                    imageButtonPlay.setImageResource(R.drawable.ic_pause);
                }
                SetTimeSum();
                UpdateTimeSong();
                imageViewCD.startAnimation(animation);
            }
        });
        seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBarTime.getProgress());
            }
        });
    }

    private void AddSong() {
        arraySong = new ArrayList<>();
        arraySong.add(new Song("Mưa", R.raw.mua));
        arraySong.add(new Song("Thương em đến già", R.raw.thuong_em_den_gia));
        arraySong.add(new Song("Mặt mộc", R.raw.mat_moc));
    }

    private void initView() {
        seekBarTime = (SeekBar) findViewById(R.id.seek_bar);
        textViewTitle = (TextView) findViewById(R.id.text_view_title);
        textViewTimeRun = (TextView) findViewById(R.id.text_view_time_run);
        textViewTimeSum = (TextView) findViewById(R.id.text_view_sum_time);
        imageButtonPrev = (ImageButton) findViewById(R.id.image_button_prev);
        imageButtonPlay = (ImageButton) findViewById(R.id.image_button_play);
        imageButtonStop = (ImageButton) findViewById(R.id.image_button_stop);
        imageButtonNext = (ImageButton) findViewById(R.id.image_button_next);
        imageViewCD = (ImageView) findViewById(R.id.image_view_cd);
    }

    private void UpdateTimeSong() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat dingdangGio = new SimpleDateFormat("mm:ss");
                textViewTimeRun.setText(dingdangGio.format(mediaPlayer.getCurrentPosition()));
                //update progress Seekbartime
                seekBarTime.setProgress(mediaPlayer.getCurrentPosition());
                //kiem tra thoi gian bai hat -> neu ket thuc -> next
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer1) {
                        position++;
                        if (position > arraySong.size() - 1) {
                            position = 0;
                        }
                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                        }
                        MediaPlayer();
                        mediaPlayer.start();
                        imageButtonPlay.setImageResource(R.drawable.ic_pause);
                        SetTimeSum();
                    }
                });
                handler.postDelayed(this, 500);
            }
        }, 100);
    }

    private void SetTimeSum() {
        SimpleDateFormat dinhdanggio = new SimpleDateFormat("mm:ss");
        textViewTimeSum.setText(dinhdanggio.format(mediaPlayer.getDuration()));
        seekBarTime.setMax(mediaPlayer.getDuration());
    }

    private void MediaPlayer() {
        mediaPlayer = MediaPlayer.create(MainActivity.this, arraySong.get(position).getFile());
        textViewTitle.setText(arraySong.get(position).getTitile());
    }
}
//Vvuxtoo